FROM debian:bookworm

RUN useradd -m subplot

RUN apt update && apt dist-upgrade -y && apt install -y curl flake8 black git build-essential sudo tidy musl musl-dev musl-tools pkg-config libssl-dev

# Enable running with `sudo` for build dependency handling during builds
RUN echo 'subplot ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# From here on down, act as our shiny new subplot user
USER subplot

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > /tmp/rustup-init.sh

RUN bash /tmp/rustup-init.sh --default-toolchain stable --profile minimal -y

RUN rm /tmp/rustup-init.sh

ENV PATH=/home/subplot/.cargo/bin:$PATH

RUN rustup component add clippy rustfmt
RUN rustup target add x86_64-unknown-linux-musl

# Now some tools we may want

RUN cargo install cargo-tarpaulin cargo-outdated cargo-minimal-versions cargo-hack cargo-deny cargo-udeps

# Now add nightly because cargo minimal-versions may need it

RUN rustup toolchain install nightly --profile minimal

# Now add the MSRV for Subplot

RUN rustup toolchain install 1.75 --component clippy --component rustfmt --target x86_64-unknown-linux-musl
RUN rustup toolchain install 1.79 --component clippy --component rustfmt --target x86_64-unknown-linux-musl

# Acquire the subplot source code and do some faffing

RUN cd /tmp && git clone https://gitlab.com/subplot/subplot

# Change this hash if the build dependencies change, this will trigger fresh
# build dep acquisition and fresh crate downloading.  Last updated 2023-12-27

RUN cd /tmp/subplot && git remote update && git checkout d7d4a575e0d09e05928eba1180b49211149d49af
RUN cd /tmp/subplot && sudo apt build-dep -y .

# Now acquire the cargo index and deps to hopefully speed subsequent builds
# at least a little...

RUN cd /tmp/subplot && cargo fetch --locked

RUN rm -rf /tmp/subplot

# And the final apt cleanup since this won't be useful for
# further runs of apt most likely anyway.

RUN sudo apt clean
RUN sudo apt autoremove
